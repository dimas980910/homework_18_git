﻿#include <iostream>
using namespace std;

class Stack
{
public:   
    
    Stack()
    {
        v = new int();
        head = -1;
    }

    ~Stack() 
    {
        delete [] new int;
    }
    
    void push(int elem)
    {
        head++;
        v[head] = elem;
        cout << "pushed: " << elem << endl;
    }

    int pop()
    {
        if (head != -1)
        {
            head--;
            cout << "poped: " << v[head + 1] << endl;
            return v[head + 1];
        }
        else
        {
            return 0; //Ошибка, стек пуст.
        }
    }

    void show()
    {
        cout << "   stack: ";
        for (int i = 0; i <= head; i++)
        {
            cout << v[i] << " ";
        }
        cout << endl;
    }
private:
    int* v;
    int head; //Индекс крайнего элемента
};

int main()
{
    Stack stack1;
    stack1.push(1);
    stack1.push(2);
    stack1.push(3);
    stack1.show();
    stack1.pop();
    stack1.show();
    stack1.push(4);
    stack1.show();
    stack1.push(5);
    stack1.push(6);
    stack1.show();

    return 0;
}